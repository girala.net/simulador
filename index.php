<html>

<body>

<h1>Simulador de dinàmica de la moneda social</h1>

<p>
La idea és agafar un "nucli" a on es vol implementar la moneda, per exemple un poble/comarca, 
un barri d'una ciutat, etc. I estimar quin tipus de canvi €/g1 seria el més adequat, depenent de les 
dinàmiques de comerç, nombre de consumidors, moneda disponible, etc.
</p>

<h2>Paràmetres fixes (de moment)</h2>

<ul>
	<li>Estic comptant que cada més s'usa la metitat de capital disponible per part dels clients per fer compres. 
De moment no ho he posat com a paràmetre, però podria. (cal fer el canvi també a la llegenda)</li>
</ul>

<?php

// Valors incials

$canvi=10; //g1/€
$creadors=5;
$capitalInicialG1=2000;
$botiguesInicials=15;
$creadorsNousPercentatge=10;
$botiguesNovesPercentatge=5;
$acceptacioPercentatge=15;
$transaccioImport=20;
$transaccionsBotiguersPercentatgePercentatge=1;
$mesFinal=24;




// Canviem defectes per valors acabats d'enviar
if($_POST) {
	$canvi=(int)$_POST["canvi"];
	$creadors=(int)$_POST["creadors"];
	$creadorsNousPercentatge=(int)$_POST["creadorsNousPercentatge"];
	$capitalInicialG1=(int)$_POST["capitalInicialG1"];	
	$botiguesInicials=(int)$_POST["botiguesInicials"];
	$botiguesNovesPercentatge=(int)$_POST["botiguesNovesPercentatge"];
	$acceptacioPercentatge=(int)$_POST["acceptacioPercentatge"];
	$transaccioImport=(int)$_POST["transaccioImport"];
	$transaccionsBotiguersPercentatgePercentatge=(int)$_POST["transaccionsBotiguersPercentatgePercentatge"];
	$mesFinal=(int)$_POST["mesFinal"];
}

// Post-processament de les variables inicials per evitar decimals, etc.
$capitalInicial=conversorG1Euros($capitalInicialG1);
$creadorsNous=$creadorsNousPercentatge/100;
$botiguesNoves=$botiguesNovesPercentatge/100;
$acceptacio=$acceptacioPercentatge/100;
$transaccionsBotiguersPercentatge=$transaccionsBotiguersPercentatgePercentatge/100;

?>

<!-- 
	Formulari per introduir els paràmetres de la simulació ******************
-->

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>"/>

	<p>Canvi: <input type="text" name="canvi" value="<?php echo $canvi; ?>" /> g1/€</p>

	<p>Usuaris creadors de moneda inicials: <input type="text" name="creadors" value="<?php echo $creadors; ?>" /></p>

	<p>Percentatge de nous creadors cada mes, respecte els existents: <input type="text" name="creadorsNousPercentatge" 
	value="<?php echo $creadorsNousPercentatge; ?>" /> %
	<br>Nota: si no arriba a 1, es crearà igualment 1.	
	</p>

	<p>Capital inicial per dinamitzar l'activitat: <input type="text" name="capitalInicialG1" 
	value="<?php echo $capitalInicialG1; ?>" /> g1</p>

	<p>Comerços/serveis inicials: <input type="text" name="botiguesInicials" 
	value="<?php echo $botiguesInicials ?>" /></p>

	<p>Percentatge de noves botigues cada mes, respecte les existents: <input type="text" name="botiguesNovesPercentatge" 
	value="<?php echo $botiguesNovesPercentatge; ?>" /> %
	<br>Nota: si no arriba a 1, es crearà igualment 1.	

	<p>Transacció mitjana total (incloent euros): <input type="text" 
	name="transaccioImport" value="<?php echo $transaccioImport; ?>" /> G1+€</p>

	<p>Percentatge accepció moneda per part dels comerços (la resta, es paga en €): <input type="text" 
	name="acceptacioPercentatge" value="<?php echo $acceptacioPercentatge; ?>" /> %</p>
	
	<p>Transaccions mensuals dels botiguers. Percentatge de capital 
gastat depenent del nombre de botigues disponibles, entenent que a més botigues
més possibilitat hi ha de gastat-lo (defecte 1% del capital total de les botigues per cada 
botiga al circuit; si aquest valor és un 1% hi hi ha 25 botigues, es gastarà el 25% 
del capital acumulat per les botigues en compres entre elles): <input type="text" 
	name="transaccionsBotiguersPercentatgePercentatge" value="<?php echo $transaccionsBotiguersPercentatgePercentatge; ?>" /> %</p>
	
	<p>Mesos a calcular: <input type="text" name="mesFinal" value="<?php echo $mesFinal; ?>" /></p>
	
	<p><input type="submit" value="Executa la simulació">
	<button><a href="https://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']; ?>">Valors per defecte</a></button>

</form>


<h2>Pendent</h2>

<ul>
	<li>Percentatge de transaccions als comerços més usats (defecte 50%) (NO IMPLEMENTAT ENCARA)</li>
	<li>Comerços més usats (defecte 1/3 del total)  (NO IMPLEMENTAT ENCARA)</li>
	<li>A partir de determinat nombre de mesos, no mostrar-los tots, per fer més usable tot plegat</li>
	<li>Oferir-lo a la comunitat (traducció a l'anglès/francés)</li>
	<li>Potser caldria limitar els paràmetres que poden carregar massa els càlculs, com el percentatge de botigues noves</li>
	<li>Per evitar valors disparatats, o que forcen el procés de càlcul i peten l'scrip, potser seria millor tenir camps 
	de selecció en comptes de camps oberts</li>
	<li>Caldria poder activar un log amb les transaccions per poder validar que els càlculs estan bé</li>
</ul>

<h3>Errors</h3>
<ul>
<li>En canvis superiors a 50 peta</li>
</ul>


<?php

// Les botigues tindran una columna per l'import en g1 i la segona per l'ingrés en €
$botigues=array();
for($i=0;$i<$botiguesInicials;$i++) {
  $botigues[$i][0]="0";
  $botigues[$i][1]="0";
}

function conversorEurosG1($euros){
	global $canvi;
  return $g1 = $euros * $canvi;//*=$canvi;
}

function conversorG1Euros($g1){
	global $canvi;
  return $euros = $g1 / $canvi;//*=$canvi;
}



/*
  $test = conversorEurosG1("100");
echo "<p>Test a veure a quants g1 em passa 100€ " . $test;
  $test2 = conversorG1Euros("100");
echo "<p>Test a veure a quants € em passa 100g1 " . $test2;
*/

/****************************************
  Iteració mensual 
****************************************/

for($mes=0;$mes < $mesFinal;$mes++) {
	
  echo "<h2>Mes $mes</h2>";
  $monedaCreada=conversorG1Euros($creadors*300);
  echo "<br>Moneda creada: " . $monedaCreada . " (en la seva equivalència a euros)";
	$capitalInicial+=$monedaCreada;
  echo "<br>Capital dels clients (moneda creada + capital inicial restant): $capitalInicial (en la seva equivalència a euros)";
  
  // Taula de dades de les botigues
  echo "<h3>Botigues:</h3><table border=1><tr><th>#</th><th>g1</th><th>euros</th></tr>";
  $fila=1;
  foreach($botigues as $botiga){
	 echo "<tr><td> $fila </td>";
    echo "<td> $botiga[0] </td>";
    echo "<td> $botiga[1] </td></tr>";
    //$botigues[$fila-1][0]+="20";
    $fila++;
  }
  echo "</table>";

  $nombreBotigues=count($botigues);
  echo "<br>Nombre de botigues: " . $nombreBotigues; 
  $g1BotiguesTotals= array_sum(array_column($botigues,0));
  $euroBotiguesTotals= array_sum(array_column($botigues,1));

	/*********************************
	* Transaccions 
	************************************/
	
  //Transaccions amb el capital disponible en el sistema
  $transaccionsCapitalInicial=($capitalInicial/($transaccioImport*$acceptacio))/2;
  echo "<br>Transaccions amb una meitat part del capital inicial restant: $transaccionsCapitalInicial";
  for($i=0;$i<$transaccionsCapitalInicial;$i++){
  	   $botiga=rand(0,$nombreBotigues-1);
      $botigues[$botiga][0]+=$transaccioImport*$acceptacio; //els g1
      $botigues[$botiga][1]+=$transaccioImport*(1-$acceptacio); // els euros
  }  
   

  //Transaccions botiguers
  $transaccionsBotiguers = round($transaccionsBotiguersPercentatge * $nombreBotigues * $g1BotiguesTotals / $transaccioImport);
  echo "<br>moneda lliure equivalents a euro botigues en total: " . $g1BotiguesTotals;
  echo "<br>Euros botigues en total: " . $euroBotiguesTotals;
  $rendiment = $euroBotiguesTotals/$g1BotiguesTotals;
  echo "<br>Rendiment (euros guanyats per cada moneda lliure equivalent generat): " . $rendiment;
  echo "<br>Transaccions dels botiguers: $transaccionsBotiguers ";

  for($i=0;$i<$transaccionsBotiguers;$i++){
		// botiga venedora
  	   $botiga=rand(0,$nombreBotigues-1);
      $botigues[$botiga][0]+=$transaccioImport*$acceptacio;  // els g1
      $botigues[$botiga][1]+=$transaccioImport*(1-$acceptacio); // els euros
		// botiga compradora
      $botigaCompradora=rand(0,$nombreBotigues-1);
      while($botigues[$botigaCompradora][0]<= $transaccioImport*$acceptacio) {
          $botigaCompradora=rand(0,$nombreBotigues);
      }
      $botigues[$botigaCompradora][0]-=$transaccioImport*$acceptacio;

  }  

 /*  OBSOLET 2021-06-02: a partir d'ara separarem les transaccions del consumidors de les dels botiguers consumidores
  //Transaccions totals
 // $transaccions= $transaccionsBotiguers+$transaccionsCapitalInicial;

  for($i=0;$i<$transaccions;$i++){
  	   $botiga=rand(0,$nombreBotigues-1);
      $botigues[$botiga][0]+=$transaccioImport*$acceptacio;
		if($capitalInicial>=$transaccioImport) {
        $capitalInicial-=$transaccioImport*$acceptacio;
      }
		//Ja no queda capital al circuit i s'usa el de les botigues
      else {
      	$botigaCompradora=rand(0,$nombreBotigues-1);
      	while($botigues[$botigaCompradora][0]<= $transaccioImport*$acceptacio) {
      	    $botigaCompradora=rand(0,$nombreBotigues);
      	}
         $botigues[$botigaCompradora][0]-=$transaccioImport*$acceptacio;
      }
      $botigues[$botiga][1]+=$transaccioImport*(1-$acceptacio); // els euros
  }  
	*/  
  
  // cal afegir un %botigues noves
  $cocientBotigues=round($nombreBotigues*$botiguesNoves);
  if($cocientBotigues<=1) {
    array_push($botigues, array("0","0"));  
  }  
  else {
    for($i=0;$i<$cocientBotigues;$i++){
      array_push($botigues, array("0","0")); 
    }
  }
  
  //cal afegir nous creadors
  echo "<br>Creadors de moneda: " . $creadors;
  $cocientCreadors=round($creadors*$creadorsNous);
  if($cocientCreadors<=1) {
    $creadors++;  
  }  
  else {
		$creadors+=$cocientCreadors;	 
  }

}
?>


</body>





</html>
